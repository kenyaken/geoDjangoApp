// initialize the map coplass
  var map = L.map("map").setView([-1.2863899, 36.8172235], 10);
  map.zoomControl.setPosition("topright");
  // add osm tilelayer
  L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }).addTo(map);
  // add marker to the centre of the map
  L.marker([-1.2863899, 36.8172235])
    .addTo(map)
    .bindPopup("Nairobi,Kenya.<br> Karibu KANAIRO!.")
    .openPopup();
  // Add scaling properties
  L.control.scale({ position: "bottomright" }).addTo(map);
  //   function for fullscreenview
  var mapId = document.getElementById("map");
  function fullScreenView() {
    mapId.requestFullscreen();
  }
  //   mapfunction to get the coordinates of the position hovered on
//   function mouseMove(){
//       map.on('mousemove',function(e){
//           const latd = e.latlng.lat;
//           const longd = e.latlng.lng;
//         return latd,longd;
//       })
//   }
  map.on('mousemove',function(e){
      console.log(e.latlng.lat)
      $('.coordinate').html('Lat: &{ e.lngLat.lat } Lon: ${e.lngLat.lng}')
      
  })
  $('.print-map').click(function(){
      window.print()
  });
//   implementation of printing/saving the map 
  L.control.browserPrint().addTo(map)
//   leaflet measure functionality
  L.control.measure({
      primaryLengthUnit: 'meters',
      primaryAreaUnit: 'acres'
  }).addTo(map);
  // Load GeoJson Data
  /* initialize the marker group */
  var marker = L.markerClusterGroup();
  // load your geojson data
  var myGeojson = L.geoJSON(d);
  // add your data to the marker
  myGeojson.addTo(marker);
  // visualize everything on the map
  marker.addTo(map);
  // search/geocoder
  L.Control.geocoder().addTo(map);