from django.db import models
import datetime
from sqlalchemy import *
from geoalchemy2 import Geometry, WKTElement
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver 
import geopandas as gpd
import glob
import zipfile
import os
from geo.Geoserver import Geoserver
from geo.Postgres import Db

# initialize the library here
db = Db(dbname="geoApp", user='postgres',password='kipchumba95',host='localhost', port = '5432')
geo = Geoserver('http://127.0.0.1:8080/geoserver', username='admin', password='kipchumba95')
# Create your models here.
class Shp(models.Model):
    name = models.CharField(max_length = 100)
    description = models.CharField(max_length =1000,blank=True)
    file = models.FileField(upload_to='shapefiles')
    date_of_upload = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

'''
    after creating your models, establish also instances for publishing and
    deletion of your data at an instance.
    use @ receiver decorator to create your signals
'''
@receiver(post_save, sender=Shp)
def publish_data(sender, instance, created, **kwargs):
    file = instance.file.path #gives you the path of the current file being transacted
    file_format =os.path.basename(file).split('.')[-1]
    file_name = os.path.basename(file).split('.')[0]
    name = instance.name
    file_path = os.path.dirname(file)
    print(file,'file')
    print(file_name)
    
    conn_str = 'postgresql://postgres:kipchumba95@localhost:5432/geoApp'
    # extrax=ct zipped file
    with zipfile.ZipFile(file,'r') as zip_ref:
        zip_ref.extractall(file_path)

    
    os.remove(file)
    print("Reading Shapefile")
    # glob.glob() USED TO RECUSRSIVELY search through the file for files with certain wildcard i.e .txt, shps etc
    # return the whole path of the wild card supplied
    shp = glob.glob(r'{}/**/*.shp'.format(file_path), recursive=True)[0] # {} is a placeholder for the file path // (/**/*.) means search recusrsively for all files with . extension
    print('.....................')
    print(shp,'file')
    print('.....................')
    if isinstance(shp,str):
        print("Successfully found shp")
    else:
        print("this is not a shp")
    # geopandas used to manipulate geospatial data
    gdf = gpd.read_file(shp) # read more on its documentation

    crs_name = str(gdf.crs.srs)
    print(crs_name, 'crs_name')

    epsg = int(crs_name.replace('epsg:',''))
    if epsg is None:
        epsg= 4326
    
    geom_type = gdf.geom_type[1]
    engine = create_engine(conn_str) #  /***create sqlalchemy engine to use***/
    gdf['geom'] = gdf['geometry'].apply(lambda x: WKTElement(x.wkt,srid=epsg))
    gdf.drop('geometry',1, inplace=True) #drops geometry column
    # writes records stored on the geodataframe to the database
    gdf.to_sql(name,engine, 'data', if_exists ='replace', index= False, dtype ={'geom':Geometry('Geometry',srid=epsg)}) # create schema data to store the data
    # initialize the geoserver library 
    
    

    '''
    publish shapefiles to Geoserver
    '''
    geo.create_featurestore(store_name='geoApp',  workspace='geoapp', db='geoApp', host='localhost', pg_user='postgres',
                        pg_password='kipchumba95',schema='data')
    geo.publish_featurestore(workspace='geoapp', store_name='geoApp', pg_table=name)
@receiver(post_delete, sender=Shp)
def delete(sender, instance,**kwargs):
    db.delete_table(table_name=instance.name, schema='data')
    geo.delete_layer(layer_name=instance.name,workspace ='geoapp')