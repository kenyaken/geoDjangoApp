from django.shortcuts import render

# Create your views here.
def index(request):
    # remember to add render to your request
    return render(request,'index.html')